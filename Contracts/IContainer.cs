﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Acrotech.PortableIoCAdapter
{
    public interface IContainer
    {
        void Register<T>(Type type, Func<object[], T> creator);

        void Register<T>(Type type, T instance);

        object Compose(Type type, params object[] args);
    }
}

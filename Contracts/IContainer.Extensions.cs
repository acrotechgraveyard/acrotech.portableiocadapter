﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Acrotech.PortableIoCAdapter
{
    public static partial class ExtensionMethods
    {
        public static void Register<T>(this IContainer source, Func<object[], T> creator)
        {
            source.Register(typeof(T), creator);
        }

        public static void Register<T>(this IContainer source, Func<T> creator)
        {
            source.Register(typeof(T), _ => creator());
        }

        public static void Register<T, TReturn>(this IContainer source, Func<TReturn> creator) where TReturn : T
        {
            source.Register(typeof(T), _ => creator());
        }

        public static void Register<T, TReturn>(this IContainer source, Func<object[], TReturn> creator) where TReturn : T
        {
            source.Register(typeof(T), creator);
        }

        public static void Register<T>(this IContainer source, T instance)
        {
            source.Register(typeof(T), instance);
        }

        public static void Register<T, TInstance>(this IContainer source, TInstance instance) where TInstance : T
        {
            source.Register(typeof(T), instance);
        }

        public static void Register<T>(this IContainer source)
        {
            source.Register(typeof(T), CreateNew<T>);
        }

        public static void Register<T>(this IContainer source, params Type[] paramTypes)
        {
            source.Register(typeof(T), x => CreateNew<T>(paramTypes, x));
        }

        public static T CreateNew<T>(params object[] args)
        {
            return CreateNew<T>(args.Select(x => x.GetType()).ToArray(), args);
        }

        public static T CreateNew<T>(Type[] paramTypes, object[] args)
        {
            var ctor = typeof(T).GetConstructor(paramTypes);

            return (T)ctor.Invoke(args);
        }

        public static T Compose<T>(this IContainer source, params object[] args)
        {
            return (T)source.Compose(typeof(T), args);
        }

        public static T ComposeOrDefault<T>(this IContainer source, params object[] args)
        {
            return ComposeOrDefault(source, default(T), args);
        }

        public static T ComposeOrDefault<T>(this IContainer source, T defaultValue, params object[] args)
        {
            var result = (T)source.Compose(typeof(T), args);

            if (EqualityComparer<T>.Default.Equals(result, default(T)))
            {
                result = defaultValue;
            }

            return result;
        }
    }
}

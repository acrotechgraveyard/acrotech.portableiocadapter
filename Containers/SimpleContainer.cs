﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Acrotech.PortableIoCAdapter.Containers
{
    public class SimpleContainer : IContainer
    {
        public static readonly SimpleContainer Default = new SimpleContainer();

        public SimpleContainer()
        {
            Map = new Dictionary<Type, IResolver>();
        }

        private Dictionary<Type, IResolver> Map { get; set; }

        private void AddOrReplaceResolver(Type type, IResolver resolver)
        {
            if (Map.ContainsKey(type))
            {
                Map[type] = resolver;
            }
            else
            {
                Map.Add(type, resolver);
            }
        }

        private IResolver GetResolver(Type type)
        {
            IResolver resolver = null;

            Map.TryGetValue(type, out resolver);

            return resolver;
        }

        #region IContainer Members

        public void Register<T>(Type type, Func<object[], T> creator)
        {
            AddOrReplaceResolver(type, new DelegateResolver<T>(creator));
        }

        public void Register<T>(Type type, T instance)
        {
            AddOrReplaceResolver(type, new InstanceResolver<T>(instance));
        }

        public object Compose(Type type, params object[] args)
        {
            var resolver = GetResolver(type);

            return resolver == null ? null : resolver.Resolve(args);
        }

        #endregion

        interface IResolver
        {
            object Resolve(params object[] args);
        }

        class DelegateResolver<T> : IResolver
        {
            public DelegateResolver(Func<object[], T> creator)
            {
                Delegate = creator;
            }

            public Func<object[], T> Delegate { get; private set; }

            #region IResolver Members

            public object Resolve(params object[] args)
            {
                return Delegate(args);
            }

            #endregion
        }

        class InstanceResolver<T> : IResolver
        {
            public InstanceResolver(T instance)
            {
                Instance = instance;
            }

            public T Instance { get; private set; }

            #region IResolver Members

            public object Resolve(params object[] args)
            {
                return Instance;
            }

            #endregion
        }
    }
}

﻿# README #

This library is intended to be a drop in replacement for an abstracted IoC interface. This library provides a very basic (and likely non-performant) portable IoC container implementation, but would best be used with a platform dependent full featured IoC container.

This library is currently very primitive and offers no abstraction of complex IoC functions, only registration (instance, lambda, and generic type) and composition. If there is demand (or pull requests) for more complex abstractions I am more than happy to include these in this library.

### What is this repository for? ###

* Anyone who is writing a cross platform application and wants to include cross platform IoC and dependency injection with minimal non-shared code.

### How do I get set up? ###

0. Include this library
0. Implement the custom IContainer and wire it up to the platform dependent IoC container

### Who do I talk to? ###

* Contact me for suggestions, improvements, or bugs

### Changelog ###

#### 1.0.0.1 ####

* Using Acrotech.Versioning for versioning
* Improved NuGet pack script

#### 1.0.0.0 ####

* Initial Release
